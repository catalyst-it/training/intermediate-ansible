### Collections
#### Part 2


#### Writing collections
* <!-- .element: class="fragment" data-fragment-index="0" -->
  As of 2.10, _collections_ are how Ansible code is bundled and distributed
  - roles
  - modules
  - plugins
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Similar pattern to roles
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Handy to organise code for a specific project


#### Creating a collection
* <!-- .element: class="fragment" data-fragment-index="0" -->
  A collection name requires two components separated by a **dot**
  ```
  <namespace>.<collection>
  ```
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Namespace
  - Generally name of company/organisation
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Collection
  - The name of actual collection you are creating


#### Exercise: Create a collection
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Use `ansible-galaxy` to create a collection
  ```
  ansible-galaxy collection init <namespace>.<collection>
  ```
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Strict naming convention
  - lowercase
  - no hyphen
  - underscores allowed
  - must be `<namespace>.<collection>`


#### Example collection
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Create a sample collection
  ```
  cd $HOME
  ansible-galaxy collection init my_company.ansible_training
  ```
  <!-- .element: style="font-size:10pt;" -->
  ```
  - Collection my_company.ansible_training was created successfully
  ```
  <!-- .element: class="fragment" data-fragment-index="1" style="font-size:10pt;" -->
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Contents of collection
  ```
  my_company
  └── ansible_training
      ├── docs
      ├── galaxy.yml
      ├── plugins
      │   └── README.md
      ├── README.md
      └── roles
  ```
  <!-- .element: style="font-size:10pt;"  -->
* <!-- .element: class="fragment" data-fragment-index="3" -->
  Ansible galaxy created namespace folder and one collection


#### Example collection
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Typically your organisation would create all collections under same
  namespace
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Expand my_company namespace by adding a new collection
  ```
  cd $HOME
  ansible-galaxy collection init my_company.docker_training
  ```
  <!-- .element: style="font-size:10pt;"  -->
* <!-- .element: class="fragment" data-fragment-index="2" -->
  This creates a new collection under namespace
  ```
  my_company
  ├── ansible_training
  └── docker_training
      ├── docs
      ├── galaxy.yml
      ├── plugins
      │   └── README.md
      ├── README.md
      └── roles
  ```
  <!-- .element: style="font-size:9pt;"  -->


#### Implementing collections
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Code for the collection belongs in specific directories
  - plugins
  - roles
* <!-- .element: class="fragment" data-fragment-index="1" -->
See [documentation](https://docs.ansible.com/ansible/latest/dev_guide/developing_collections.html) about developing collections


#### Implementing roles in collections
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Roles in a collection identical structure as roles discussed earlier
  ```
  my_company
  ├── ansible_training
  │   └── roles
  │       ├── another_role
  │       │   ├── defaults
  │       │   ├── meta
  │       │   └── tasks
  │       └── my_role
  │           ├── defaults
  │           ├── meta
  │           ├── tasks
  │           ├── templates
  │           └── vars
  └── docker_training
      └── roles
          ├── my_role
          │   ├── defaults
          │   └── tasks
          └── setup_docker
              ├── defaults
              └── tasks
  ```
  <!-- .element: style="font-size:10pt;"  -->


#### Installing a collection
* <!-- .element: class="fragment" data-fragment-index="0" -->
  As with roles, `ansible-galaxy` can be used to install collections

* <!-- .element: class="fragment" data-fragment-index="1" -->
  From Ansible Galaxy
  ```
  ansible-galaxy collection install newswangerd.collection_demo
  ```
  <!-- .element: style="font-size:11pt;"  -->
* <!-- .element: class="fragment" data-fragment-index="2" -->
  From a git repository
  ```
  ansible-galaxy collection install git+https://github.com/organization/repo_name.git,devel
  ```
  <!-- .element: style="font-size:11pt;"  -->
* <!-- .element: class="fragment" data-fragment-index="3" -->
  From a private respository
  ```
  ansible-galaxy collection install git@github.com:my_company/ansible_training.git
  ansible-galaxy collection install git@github.com:my_company/docker_training.git
  ```
  <!-- .element: style="font-size:11pt;"  -->


#### Collection paths
* Default location for collection installs and where ansible looks
  ```
  ~/.ansible/collections
  ```
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Can override in `ansible.cfg`
  ```
  [defaults]
  collections_path = ansible
  ```


#### Using collections
* <!-- .element: class="fragment" data-fragment-index="0" -->
  To use an installed collection use the FQCN
  ```
  name: Sample playbook
  hosts: myserver
  roles:
    - role: my_company.docker_training.my_role
    - role: my_company.docker_training.another_role
  ```
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Can be simplified by specifying a `collections` attribute
  ```
  name: Sample playbook
  hosts: myserver
  collections:
    - my_company.docker_training
  roles:
    - role: my_role
    - role: another_role
  ```


#### Collections and namespaces
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Namespaces are important to avoid collisions between collections
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Two collections both have role with the same name but may perform very
  different tasks
* <!-- .element: class="fragment" data-fragment-index="2" -->
  `my_role` in both collections
  ```
  name: Set up docker training
  hosts: myserver
  roles:
    - role: my_company.docker_training.my_role
  
  name: Set up ansible
  hosts: myserver
  roles:
    - role: my_company.ansible_training.my_role
  
  ```


#### Summary
* Collections big part of Ansible 2.10 packaging distribution overhaul
* As with roles, collections are a way to bundle Ansible code for
  distribution
* Related roles and/or plugins can be distributed using version control and
* Installed using Ansible galaxy


