## Course Outline


* [Task conditions](#/3)
  * Interpreting and controlling errors
    - ignoring errors
  * Manipulating task conditions
    - Errors
    - Changed state
  * Error recovery
    - block/rescue


* [Organising Infrastructure Code](#/4)
    * Including playbooks
    * Including tasks
    * Passing variables to includes
    * Blocks


* [Introduction to Roles](#/5)
   * How to write a role


* [Roles part 2](#/6)
  * Importing roles
  * Distributing your own roles


* [Introduction to collections](#/7)


* [Testing Ansible](#/8)
  * Molecule
  * Testinfra


<!--* [Deploying code](deploying-code.md)-->
  <!--*  Deploying loadbalanced applications-->
  <!--*  Ansible via a bastion host-->

<!--* [Set Theory in Ansible](group-set-theory.md)-->
  <!--*  Set theory filters-->
  <!--*  Inventory set theory operators-->

<!--* [Upgrade strategies](upgrade-strategies-pt1.md)-->
  <!--*  What can go wrong?-->
  <!--*  Types of strategies-->

<!--* [Dynamic Inventories](dynamic-inventories.jd)-->
<!--* [Handling Failure](failing-fast.md)-->

<!--* [Rolling Upgrade Demo](rolling-upgrade-demo.md)-->
* [Wrap Up](#/7)
